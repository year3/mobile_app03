import 'dart:io';

bool isPrime(Number) {
  for (var i = 2; i <= Number / i; ++i) {
    if (Number % i == 0) {
      return false;
    }
  }
  return true;
}

void main() {
  print("Input number ");
  var Number = int.parse(stdin.readLineSync()!);
  if (isPrime(Number)) {
    print('$Number is a prime number.');
  } else {
    print('$Number is not a prime number.');
  }
}
